<?php
use components\basic\c\basic_page_controller;

class dispatcher {
    protected $action;

    function __construct(){
        $this -> controller = new basic_page_controller();
      
        switch ($_REQUEST['action']) {
            case 'auth':
                $this->auth($_REQUEST['token']);
                break;
            case 'auth_rev':
                $this->auth_rev($_REQUEST['token']);
                break;
            case 'cap':
                $this->cap($_REQUEST['token']);
                break;
            case 'cap_rev':
                $this->cap_rev($_REQUEST['token']);
                break;
            default:
                $this->index();
                break;
        }

    }

}
