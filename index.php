<?php
include_once 'configs/config.php';

include "POSAPI.php";

//8220276806689 
//posadmin 
//ADMoLUW
class index extends dispatcher {

    function __construct(){
        parent::__construct();
    }    

    function index(){
        $this->controller->view("<h3>aPure Payment API is up and running ...</h3>");
    }

    function auth($token){
        $Server = array(
            'HOST' => 'testepos.ctbcbank.com',
            'PORT' => '2011',
            'CAFile' => dirname(__FILE__)."\\server.cer",
            'Timeout' => 30
        );
  
        // 'LID-M' => 'ABCD000006',
        // 'PAN' => '4123451122330003123',
        // 'ExpDate' => '201712',
        // 'purchAmt' => '2000',

        $order_number = $_REQUEST['order_number'];
        
        $pan = $_REQUEST['pan_no1']
                .$_REQUEST['pan_no2']
                .$_REQUEST['pan_no3']
                .$_REQUEST['pan_no4']
                .$_REQUEST['cvc2'];

        $expdate = $_REQUEST['expire_year']
                    .$_REQUEST['expire_month'];

        $amount = $_REQUEST['transaction_amount'];
        //print $amount;
        $Auth = array(
            'MERID' => '10131',
            'LID-M' => $order_number,
            'PAN' => $pan,
            'ExpDate' => $expdate,
            'currency' => '901',
            'purchAmt' =>  $amount,
            'exponent' => '0',
            'ECI' => '7',
            'ORDER_DESC' => '訂單描述',
            'TRV_DepartDay' => '',
            'TRV_MerchantID' => '',
            'TRV_Commission' => '',
            'CAVV' => '',
            'BIRTHDAY' => '',
            'SubMerchantId' => '',
            'ProductName' => '',
            'PID' => ''
        );
        $Result = AuthTransac($Server,$Auth);

        //print_r($Auth);
        //$this->controller->response($Result);
        if($Result["RespCode"] == "00" && $Result["ErrCode"] == "00"){
            $this->cap($Result);
        }
        else{
            $this->controller->view("<h3>Credit Card not Authorized ...</h3>");
        }
    }
    
    function cap($Result){
        $Server = array(
            'HOST' => 'testepos.ctbcbank.com',
            'PORT' => '2011',
            'CAFile' => dirname(__FILE__)."\\server.cer",
            'Timeout' => 30
        );
        // From auth result
        // XID

        $Cap = array(
            'MERID' => '10131',
            'XID' => $Result["XID"],
            'AuthRRPID' => $Result["AuthRRPID"],
            'currency' => $Result["currency"],
            'orgAmt' => $Result["amount"],
            'capAmt' => $Result["amount"],
            'exponent' => $Result["exponent"],
            'AuthCode' => $Result["AuthCode"],
            'TermSeq' => $Result["TermSeq"]
        );
        $Cap_Result = CapTransac($Server, $Cap);
        
        //$this->controller->response($Cap_Result);
        if($Cap_Result["RespCode"] == "00" && $Cap_Result["ErrCode"] == "00"){
            //$this->controller->view("<h3>Payment Done!! ...</h3>");
            header('Location: http://13.115.204.182/thankyou');
        }else{
            $this->controller->response($Cap_Result);
        }
    }

    function auth_rev($token){
        $Server = array(
            'HOST' => 'testepos.ctbcbank.com',
            'PORT' => '2011',
            'CAFile' => dirname(__FILE__)."\\server.cer",
            'Timeout' => 30
        );
        $AuthRev = array(
            'MERID' => '10131',
            'XID' => $Result["XID"],
            'AuthRRPID' => $Result["AuthRRPID"],
            'currency' => $Result["currency"],
            'orgAmt' => $Result["amount"],
            'authnewAmt' => 0,
            'exponent' => $Result["exponent"],
            'AuthCode' => $Result["AuthCode"],
            'TermSeq' => $Result["TermSeq"]
        );
        $Rev_Result = AuthRevTransac($Server, $AuthRev);
        $this->controller->response($Rev_Result);
    }
    

    function cap_rev($token){
        $Server = array(
            'HOST' => 'testepos.ctbcbank.com',
            'PORT' => '2011',
            'CAFile' => dirname(__FILE__)."\\server.cer",
            'Timeout' => 30
        );
        $CapRev = array(
            'MERID' => '10131',
            'XID' => $Result["XID"],
            'AuthRRPID' => $Result["AuthRRPID"],
            'currency' => $Result["currency"],
            'orgAmt' => $Result["amount"],
            'authnewAmt' => 0,
            'exponent' => $Result["exponent"],
            'AuthCode' => $Result["AuthCode"],
            'TermSeq' => $Result["TermSeq"]
        );
        $Cap_Rev_Result = AuthRevTransac($Server, $CapRev);
        $this->controller->response($Cap_Rev_Result);
    }
}

if (class_exists(index)){
    $start_classes =new index();
}
